<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    echo "<h3>RELEASE 0</h3>";
    $sheep = new Animal("shaun");

    echo "Name: ".$sheep->name.'<br>'; // "shaun"
    echo "Legs: ".$sheep->legs.'<br>'; // 2
    if($sheep->cold_blooded){
        echo 'Cold_blooded = true <br>'; // true
    }else{
        echo 'Cold_blooded = false <br>'; // false
    }

    echo "<h3>RELEASE 1</h3>";
    $sungokong = new Ape("kera sakti");
    echo "Name: ".$sungokong->name.'<br>'; // "shaun"
    echo "Legs: ".$sungokong->legs.'<br>'; // 2
    if($sungokong->cold_blooded){
        echo 'Cold_blooded = true <br>'; // true
    }else{
        echo 'Cold_blooded = false <br>'; // false
    }
    $sungokong->yell(); // "Auooo"
    echo '<br><br>';

    $kodok = new Frog("buduk");
    echo "Name: ".$kodok->name.'<br>'; // "shaun"
    echo "Legs: ".$kodok->legs.'<br>'; // 2
    if($kodok->cold_blooded){
        echo 'Cold_blooded = true <br>'; // true
    }else{
        echo 'Cold_blooded = false <br>'; // false
    }
    $kodok->jump() ; // "hop hop"


?>