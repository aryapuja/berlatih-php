<?php
function tentukan_nilai($number)
{
    if ($number >= 85 && $number <= 100) {
        $hasil = "Nilai $number masuk kategori <b> Sangat Baik </b> <br>";
    }else if ($number >= 70 && $number < 85) {
        $hasil = "Nilai $number masuk kategori <b> Baik </b> <br>";
    }else if ($number >= 60 && $number < 70) {
        $hasil = "Nilai $number masuk kategori <b> Cukup </b> <br>";
    }else{
        $hasil = "Nilai $number masuk kategori <b> Kurang </b> <br>";
    }
    return $hasil;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>