<?php
function tukar_besar_kecil($string){
$dummy="";
    for ($panjang=strlen($string)-1; $panjang >= 0; $panjang--) { 
        if(ctype_upper($string[$panjang]) == TRUE){
            $dummy = strtolower($string[$panjang]).$dummy;
        }else{
            $dummy = strtoupper($string[$panjang]).$dummy;
        }
    }
    return $dummy."<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>