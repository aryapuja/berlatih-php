<?php
function ubah_huruf($string){
   $dummy="";
    for ($panjang=strlen($string)-1; $panjang >= 0; $panjang--) { 
        $ubah = $string[$panjang];
        $ubah++;
        $dummy = $ubah.$dummy;
    }
    return $dummy."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
echo ubah_huruf('AlieZz'); // tfnbohbu

?>